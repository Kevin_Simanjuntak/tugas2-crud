from django.db import models
from django.contrib.auth.models import *

class DataDiri(models.Model):

    nama = models.CharField(max_length=128)
    alamat = models.CharField(max_length=1280)
    umur = models.PositiveIntegerField()
    jenis_kelamin = models.CharField(max_length=128)

    class Meta:
        db_table = 'DataDiri'
