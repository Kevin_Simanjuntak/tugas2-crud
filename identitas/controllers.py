#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

from django.http import HttpResponse
from django.conf import settings
#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from login.OAuth import *

from rest_framework.exceptions import PermissionDenied

import os, shutil
import shutil
from zipfile import ZipFile
from django.http import HttpResponse

real_dir = os.getcwd()
class Identitas(viewsets.ModelViewSet):
    serializer_class = DataDiriSerializer
    queryset = DataDiri.objects.all()

    def create(self, request) :
        form_serializer = DataDiriSerializer(data=request.data)
        if is_authenticated(request) :
            if form_serializer.is_valid():
                form_serializer.save()
            return Response(form_serializer.data, status=status.HTTP_201_CREATED)

        else:
            return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def list(self, request) :
        if is_authenticated(request) :
            qry = DataDiri.objects.all()
            serializer = DataDiriSerializer(qry, many=True)
            return Response({'result':serializer.data})

        else:
            return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def retrieve(self, request, pk) :
        if is_authenticated(request) :
            try:
                instance = DataDiri.objects.get(id=pk)
                serializer = DataDiriSerializer(instance)
                return Response(serializer.data)
            except:
                return Response({"Pesan ": "File tidak tersedia" }, status=status.HTTP_400_BAD_REQUEST)
        return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

        

    def update(self, request, *args, **kwargs):
        if is_authenticated(request):
            partial = kwargs.pop("partial", False)
            instance = self.get_object()
            instance.nama = request.data.get("nama")
            instance.umur = request.data.get("umur")
            instance.jenis_kelamin = request.data.get("jenis_kelamin")
            instance.alamat = request.data.get("alamat")
            instance.save()
            serializer = self.get_serializer(instance)
            if getattr(instance, '_prefetched_objects_cache', None):
                instance._prefetched_objects_cache = {}
                return Response(serializer.data)
            else:
                return Response(serializer.data)

        else:
            return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def destroy(self, request, pk) :
        if is_authenticated(request):

            try:
                instance = self.get_object()
                self.perform_destroy(instance)
            except:
                pass
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)




            